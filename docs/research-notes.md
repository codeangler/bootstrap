

## macos settings
- https://gist.github.com/millermedeiros/6615994#5-borrow-a-few-osx-settings-from-mathiasbynens-dotfiles
- https://github.com/codeangler/dotfiles-mathiasbynens/blob/main/.macos

## asdf

- see documentaton for how to setup shell
- https://alchemist.camp/episodes/asdf-language-versions


## divvy

- https://mizage.com/help/divvy/export_import.html


## app store list

https://apps.apple.com/us/app/divvy-window-manager/id413857545?mt=12
https://apps.apple.com/us/app/microsoft-remote-desktop/id1295203466?mt=12
https://apps.apple.com/us/app/wireguard/id1451685025?mt=12
https://apps.apple.com/us/app/flycut-clipboard-manager/id442160987?mt=12
https://apps.apple.com/us/app/be-focused-focus-timer/id973134470?mt=12

## boostrap with  Ansible

https://github.com/geerlingguy/mac-dev-playbook

https://adoptium.net/installation/macOS/
```
installer -pkg <path_to_pkg>/<pkg_name>.pkg -target /
```