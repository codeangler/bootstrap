#!/bin/sh

BOOTSTRAP=$1

if [ -z "$BOOTSTRAP" ]; then
    BOOTSTRAP="$HOME/bootstrap"
fi

# --------------------
# check if installed
# --------------------

if test ! $(which asdf); then
  echo -e '\n. $HOME/.asdf/asdf.sh' >> ~/.bashrc
  $HOME/.asdf/asdf.sh
  git clone https://github.com/asdf-vm/asdf.git ~/.asdf --branch v0.13.1
  echo -e "\n. $(brew --prefix asdf)/libexec/asdf.sh" >> ${ZDOTDIR:-~}/.zshrc
  source ~/.zshrc
fi

# ------------------------------
# installation
# -----------------------------

# todo prompt with loop from directory files names.

BOOTSTRAP=$1

if [ -z "$BOOTSTRAP" ]; then
    BOOTSTRAP="$HOME/bootstrap"
fi

while true; do
    echo "Minimum Init Has Completed:"
    echo "1. Install terraform"
    echo "2. Install nodejs"
    echo "3. Install google sdk"
    echo "4. Install rust-lang"
    echo "5. Install all"
    echo "6. Continue"

    read choice
    case "$choice" in
        1)
            echo "Installing terraform"
            sh $BOOTSTRAP/tools/asdf/asdf-terraform.sh
            ;;
        2)
            echo "Installing nodejs"
            sh $BOOTSTRAP/tools/asdf/asdf-nodejs.sh
            ;;
        3)
            echo "Installing googlesdk"
            sh $BOOTSTRAP/tools/asdf/asdf-google-sdk.sh
            ;;
        4)
            echo "installing rust-lang"
            sh $BOOTSTRAP/tools/asdf/asdf-rust-lang.sh
            ;;
        5)
            echo "This does nothing, try again "
           #
            ;;
        6)
            echo "Goodbye!"
            exit 0;
            break  # Exit the script
            ;;
        *)
            echo "Invalid option. Please select a valid option."
            ;;
    esac
done

# sh $BOOTSTRAP/tools/asdf/asdf-node.sh
# sh $BOOTSTRAP/tools/asdf/asdf-terraform.sh

