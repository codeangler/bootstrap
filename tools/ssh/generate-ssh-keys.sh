#!/bin/sh
# ----------------------------------------
#
#
# generate new public/private keys
# push them to applications
# TODO prompt to delete existing keys on applications
# ----------------------------------------

# GitHub API Token
if [ -z "$GH_TOKEN" ]; then
    echo "The environment variable GH_TOKEN is not set."
    echo "please generate token with necessary permissions"
    echo "read more here: https://docs.github.com/en/rest/users/keys?apiVersion=2022-11-28#create-a-public-ssh-key-for-the-authenticated-user"
    echo "enter GH_TOKEN, press return"
    read GH_TOKEN
    echo $GH_TOKEN
else
    echo "The environment variable GH_TOKEN is set."
fi

if [ ! -f "$HOME/.ssh/github_rsa" ]; then
  echo ".ssh/github_rsa does NOT exist"
  echo "Name the key $HOME/.ssh/github_rsa"
  ssh-keygen -t rsa

  GH_RSA=$(cat $HOME/.ssh/github_rsa.pub)
  echo $GH_RSA
  curl -L \
  -X POST \
  -H "Accept: application/vnd.github+json" \
  -H "Authorization: Bearer $GH_TOKEN" \
  -H "X-GitHub-Api-Version: 2022-11-28" \
  https://api.github.com/user/keys \
  -d "{\"title\":\"ssh-rsa bootstrap-1042\",\"key\": \"$GH_RSA\"}"
fi

# GitLab API Token
if [ -z "$GL_TOKEN" ]; then
    echo "The environment variable GL_TOKEN is not set."
    echo "please generate token with necessary permissions"
    echo "read more here: "
    read  GL_TOKEN
else
    echo "The environment variable GL_TOKEN is set."
fi

if [ ! -f "$HOME/.ssh/gitlab_rsa" ]; then
  echo ".ssh/gitlab_rsa does NOT exist"
  echo "Name the key :: $HOME/.ssh/gitlab_rsa"
  ssh-keygen -t rsa

  GL_RSA=$(cat $HOME/.ssh/gitlab_rsa.pub)
  echo $GL_RSA
  curl -L \
  -X POST \
  -H "Content-Type: application/json" \
  -H "PRIVATE-TOKEN: $GL_TOKEN" \
  https://gitlab.com/api/v4/user/keys \
  -d "{\"title\":\"ssh-rsa bootstrap-$(hostname)-1042\",\"key\": \"$GL_RSA\", \" usage_type\": \"auth\"}"
fi

[ ! -r ~/.zlogin ] && ln -s "$HOME/dotfiles/ssh/config" ~/.ssh/config

