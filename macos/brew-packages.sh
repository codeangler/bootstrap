#!/bin/sh

BOOTSTRAP=$1

if [ -z "$BOOTSTRAP" ]; then
    BOOTSTRAP="$HOME/bootstrap"
fi

while true; do
    echo "Minimum Init Has Completed:"
    echo "1. Apple App Store Browser Links"
    echo "2. Install tools/browsers"
    echo "3. Install cli tools/networking"
    echo "4. Install plugins from tools/vscode"
    echo "5. Continue"

    read choice
    case "$choice" in
        1)
            echo "You selected: Option 1"
            sh $BOOTSTRAP/macos/app-store.sh
            ;;
        2)
            echo "Installing Browsers"
            echo "$BOOTSTRAP/tools/brew/browsers/Brewfile"
            brew bundle --file="$BOOTSTRAP/tools/brew/browsers/Brewfile"
            ;;
        3)
            echo "Installing tools/networking"
            brew bundle --file="$BOOTSTRAP/tools/brew/networking/Brewfile"
            ;;
        4)
            echo "You selected: Option 4"
            brew bundle --file="$BOOTSTRAP/tools/brew/vscode-fonts/Brewfile"
            ;;
        5)
            echo "Goodbye!"
            exit 0;
            break  # Exit the script
            ;;
        *)
            echo "Invalid option. Please select a valid option."
            ;;
    esac
done