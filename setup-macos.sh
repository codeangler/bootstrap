#!/bin/bash

WORK="$HOME/Work"
SCRATCHPAD="$WORK/Scratchpad"
BOOTSTRAP="$HOME/bootstrap"
DOTFILES="$HOME/dotfiles"
LIFE="$HOME/Life"

# -----------------------------
#  create some directories
# -----------------------------

if [ -d "$WORK" ]; then
    echo "The $WORK directory exists."
else
    echo "Making $WORK"
    mkdir $WORK
fi

if [ -d "$SCRATCHPAD" ]; then
    echo "The $SCRATCHPAD directory exists."
else
    echo "Making $SCRATCHPAD"
    mkdir $SCRATCHPAD
fi
if [ -d "$LIFE" ]; then
    echo "The $LIFE directory exists."
else
    echo "Making $LIFE"
    mkdir $LIFE
fi

# ------------------------------
# xcode stuff
# ------------------------------

echo "1.0 Installing xcode-stuff"

if [ -x "$(command -v xcode-select)" ]; then
    echo "1.1 Xcode command-line tools are installed."
    # Check for available updates
    update_list=$(softwareupdate -l)
    echo "update_list $update_list"

    # Check if the output is empty
    if [[ "update_list == found*" ]]; then
      echo "1.2 Updates are available. Performing update..."
      sudo softwareupdate -ir
    else
      echo "1.2 No updates are available."
    fi
else
    echo "1.1 Xcode command-line tools are not installed."
    echo "1.2 installing"
    xcode-select --install
fi

# ------------------------------
# Homebrew
# ------------------------------

# Install if we don't have it
if test ! $(which brew); then
  echo "2.0 Installing homebrew..."
  NONINTERACTIVE=1 /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
  echo "2.01 source shell profiles ..."
  echo "export PATH=/opt/homebrew/bin:$PATH" >> $HOME/.zprofile
  eval $(/opt/homebrew/bin/brew shellenv) >> $HOME/.zprofile
  source $HOME/.zprofile
fi

# Update homebrew recipes
echo "2.1 Updating homebrew..."
brew update

echo "3.0 Installing Git..."
brew install git

echo "3.1 Cloning 'codeangler/bootstrap' to $BOOTSTRAP"
cd {$HOME}
if [ -d "$BOOTSTRAP" ]; then
  cd $BOOTSTRAP
  git checkout master
  git pull origin master
else
  git clone https://gitlab.com/codeangler/bootstrap.git  $BOOTSTRAP
  cd $BOOTSTRAP
fi

echo "0.0  dotfile clone"
cd "$HOME"
if [ -d "$DOTFILES" ]; then
  cd $DOTFILES
  git checkout master
  git pull origin master
else
  git clone https://gitlab.com/codeangler/dotfiles.git  $DOTFILES
  cd $DOTFILES
fi

echo "0.1  dotfile setup"
sh $DOTFILES/setup.sh $DOTFILES $BOOTSTRAP

echo "4.0 Brew install tools/brew/init/Brewfile"
brew bundle --file="$BOOTSTRAP/tools/brew/init/Brewfile"

source $HOME/.zprofile

echo "5.0 Update MacOs Settings"
bash $BOOTSTRAP/macos/config.sh

echo "6.0 Loop on Brew Packages"
bash $BOOTSTRAP/macos/brew-packages.sh  $BOOTSTRAP

echo "7.0 Loop on asdf plugins"
bash $BOOTSTRAP/tools/asdf/init.sh  $BOOTSTRAP

source $HOME/.zprofile

rm $HOME/setup-macos.sh